import { ArbdAppPage } from './app.po';

describe('arbd-app App', () => {
  let page: ArbdAppPage;

  beforeEach(() => {
    page = new ArbdAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
