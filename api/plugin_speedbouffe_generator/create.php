<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../config/database.php';

// instantiate product object
include_once '../objects/Acheteur.php';
include_once '../objects/Consommateur.php';
include_once '../objects/InfoCommande.php';
include_once '../objects/Commande.php';
include_once '../objects/Plat.php';

$database = new Database();
$db = $database->getConnection();
$plat = new Plat($db);
$tab_plats = $plat->read();
$id_tab_plats;
$i = 0;
$continue = true;
while (true)
  {
    $file = fopen('temp.json', 'r+');
    ftruncate($file, 0);
    fputs($file, `php ../generateur_speedbouffe/script.php 1`);
    fclose($file);

    $id_tab_plats = array();
    $acheteur = new Acheteur($db);
    $consommateur = new Consommateur($db);
    $commande = new Commande($db);
    $info_commande = new InfoCommande($db);

    $data = json_decode(file_get_contents("temp.json"));
    constructCustomer($acheteur, $data->Acheteur);
    saveEntity($acheteur);
    $id_tab_plats = saveConsumers($consommateur, $data->Details_commande, $tab_plats, $id_tab_plats);
    construcInfoCommande($info_commande, $data->Infos_commande);
    saveEntity($info_commande);
    construcCommande($acheteur, $consommateur, $info_commande, $commande, $data->Details_commande, $id_tab_plats);
    $i = $i + 1;
    printNbJsonGenerated($i);
  }

function printNbJsonGenerated($i) {
  echo '{';
  echo 'JSON généré n° ' . $i;
  echo '}';
}
function constructCustomer($acheteur, $data) {
  $acheteur = construcPeople($acheteur, $data);
  $acheteur->mail = $data->Email;
}

function saveEntity($entity) {
  $str = get_class($entity);
 if (!$entity->create()) {
    echo '{';
    echo 'Unable to create ' . $str;
    echo json_encode($entity);
    echo '}';
    exit(0);
  }
}

function construcPeople($people, $data) {
  $people->first_name = $data->Prenom;
  $people->last_name = $data->Nom;
  $people->age = $data->Age;
  if ($data->Civilite == 'Madame')
    {$civ = 1;}
  else if ($data->Civilite == 'Mademoiselle')
    {$civ = 2;}
  else
    {$civ = 3;}
  $people->civility = $civ;
  return $people;
}

function saveConsumers($consommateur, $data, $tab_plats, $id_tab_plats) {
  $t = 0;

  foreach ($data as $key => $value)
    {
      $c = 'Commande' . $t;
      array_push($id_tab_plats, getIdPlat($value->$c->Repas, $tab_plats));
      constructConsumer($consommateur, $value->$c, $tab_plats, $id_tab_plats);
      saveEntity($consommateur);
      $t++;
    }
  $id_tab_plats = array_reverse($id_tab_plats);
  return $id_tab_plats;
}

function constructConsumer($consommateur, $data) {
  $consommateur = construcPeople($consommateur, $data);
  $consommateur->tarif = getTarif($data->Tarif);
}

function construcInfoCommande($info_commande, $data) {
  $info_commande->date_livraison = setDate($data->Jour, $data->Horaire_livraison);
  $info_commande->type_paiement = getPaiementType($data->Paiement_espece);
}

function construcCommande($customer, $consumer, $info, $commande, $data, $id_tab_plats) {
  $last_customer = getLatests($customer, 1)[0];
  $last_consumers = getLatests($consumer, count($data));
  $last_info = getLatests($info, 1)[0];
  for ($i = 0; $i < count($last_consumers); $i++) {
    $commande->id_acheteur = $last_customer;
    $commande->id_info_commande = $last_info;
    $commande->id_consommateur = $last_consumers[$i];
    $commande->id_plat = $id_tab_plats[$i];
    saveEntity($commande);
  }
}

function getTarif($tarif) {
  if (strpos(strtoupper($tarif), "PLEIN") !== false)
    return 1;
  else if (strpos(strtoupper($tarif), "REDUIT") !== false)
    return 2;
  else if (strpos(strtoupper($tarif), "SENIOR") !== false)
    return 3;
  else if (strpos(strtoupper($tarif), "ETUDIANT") !== false)
    return 4;
}

function setDate($date, $time) {
  return ($date . ' ' . $time . ':00');
}

function getPaiementType($data) {
  if (strpos(strtoupper($data), "OUI") !== false)
    return 3;
  else
    return 1;
}

function getLatests($entity, $limit) {
  return $entity->getLatest($limit);
}

function getIdPlat($plat, $tab_plats) {
  $res = null;
  for ($i = 0; $i < count($tab_plats); $i++) {
    if ($plat == $tab_plats[$i]["label_plat"])
      $res = $tab_plats[$i]["id_plat"];
  }
  return $res;
}
?>
