<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../objects/Acheteur.php';
include_once '../objects/Consommateur.php';
include_once '../objects/InfoCommande.php';
include_once '../objects/Plat.php';

// instantiate database and acheteur object
$database = new Database();
$db = $database->getConnection();
    // select all query
    $query = "SELECT `AGE_ACHETEUR` as age,COUNT(`AGE_ACHETEUR`) nb_age
    FROM `TB_ACHETEUR`
    GROUP BY `AGE_ACHETEUR`";

    // prepare query statement
    $stmt = $db->prepare($query);

    // execute query
    $stmt->execute();
    echo json_encode(returnResult($stmt));
    //return $stmt;


function returnResult($stmt) {
    $toReturn = new ToReturn();
    $num = $stmt->rowCount();
    if($num>0){
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $toReturn = fillToReturn($row, $toReturn);
      }
      return $toReturn;
    }
    else{
	    return null;
      }
    }

function fillToReturn($row, $toReturn) {
  if ($row['age'] < 18)
    $toReturn->_18 = $row['nb_age'];
  else if ($row['age'] >= 18 && $row['age'] <= 25)
    $toReturn->_18_25 = $row['nb_age'];
  else if ($row['age'] >= 26 && $row['age'] <= 39)
    $toReturn->_26_39 = $row['nb_age'];
  else if ($row['age'] >= 40 && $row['age'] <= 54)
    $toReturn->_40_54 = $row['nb_age'];
  else 
    $toReturn->_55 = $row['nb_age'];
  
  return $toReturn;
}

class ToReturn {
    public $_18 = 0;
    public $_18_25 = 0;
    public $_26_39 = 0;
    public $_40_54 = 0;
    public $_55 = 0;
    public $str_18 = "Moins de 18 ans";
    public $str_18_25 = "De 18 à 25 ans";
    public $str_26_39 = "de 26 à 39 ans";
    public $str_40_54 = "De 40 à 54 ans";
    public $str_55 = "Plus de 55 ans";
}
/*
// initialize object
$acheteur = new Acheteur($db);
$cons = new Consommateur($db);
$info = new InfoCommande($db);
$plat = new Plat($db);
// query acheteur
$stmt = $acheteur->getLatest(1);
echo json_encode($stmt);
*/
?>