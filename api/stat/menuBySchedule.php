<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../objects/Acheteur.php';
include_once '../objects/Consommateur.php';
include_once '../objects/InfoCommande.php';
include_once '../objects/Plat.php';

// instantiate database and acheteur object
$database = new Database();
$db = $database->getConnection();
    // select all query
    $query = "SELECT COUNT(TB_INFO_COMMANDE.ID_INFO_COMMANDE) as nb_plat, TIME(TB_INFO_COMMANDE.DATE_LIVRAISON) as dte 
              FROM TB_INFO_COMMANDE 
              GROUP BY dte";

    // prepare query statement
    $stmt = $db->prepare($query);

    // execute query
    $stmt->execute();
    echo json_encode(returnResult($stmt));
    //return $stmt;


function returnResult($stmt) {
    $toReturn = new ToReturn();
    $num = $stmt->rowCount();
    if($num>0){
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        array_push($toReturn->labels, $row['dte']);
        array_push($toReturn->data, $row['nb_plat']);
      }
      return $toReturn;
    }
    else{
	    return null;
      }
    }


class ToReturn {
    public $data = array();
    public $labels = array();
}
/*
// initialize object
$acheteur = new Acheteur($db);
$cons = new Consommateur($db);
$info = new InfoCommande($db);
$plat = new Plat($db);
// query acheteur
$stmt = $acheteur->getLatest(1);
echo json_encode($stmt);
*/
?>