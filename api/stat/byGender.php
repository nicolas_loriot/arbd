<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../objects/Acheteur.php';
include_once '../objects/Consommateur.php';
include_once '../objects/InfoCommande.php';
include_once '../objects/Plat.php';

// instantiate database and acheteur object
$database = new Database();
$db = $database->getConnection();
    // select all query
    $query = "SELECT ID_CIVILITE as civ, COUNT(ID_CIVILITE) as nb_civ FROM TB_ACHETEUR GROUP BY ID_CIVILITE";

    // prepare query statement
    $stmt = $db->prepare($query);

    // execute query
    $stmt->execute();
    echo json_encode(returnResult($stmt));
    //return $stmt;


function returnResult($stmt) {
    $toReturn = new ToReturn();
    $num = $stmt->rowCount();
      //return $stmt;
      if($num>0){
	$acheteurs_arr=array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	  extract($row);
      if ($row['civ'] == 1 || $row['civ'] == 2)
        $toReturn->nb_femal += $row['nb_civ'];
      else
        $toReturn->nb_male += $row['nb_civ'];
	}
    return $toReturn;
      }
      else{
	return null;
      }
    }

    class ToReturn {
        public $nb_male = 0;
        public $nb_femal = 0;
        public $male = 'Homme';
        public $femal = 'Femme';
    }
/*
// initialize object
$acheteur = new Acheteur($db);
$cons = new Consommateur($db);
$info = new InfoCommande($db);
$plat = new Plat($db);
// query acheteur
$stmt = $acheteur->getLatest(1);
echo json_encode($stmt);
*/
?>