<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';
include_once '../objects/Acheteur.php';
include_once '../objects/Consommateur.php';
include_once '../objects/InfoCommande.php';
include_once '../objects/Plat.php';

$limit = json_decode(file_get_contents("php://input"));

// instantiate database and acheteur object
$database = new Database();
$db = $database->getConnection();
    // select all query
    $query = "SELECT TB_CONSOMMATEUR.NOM_CONSOMMATEUR as nom_conso, 
              TB_CONSOMMATEUR.PRENOM_CONSOMMATEUR as prenom_conso,
              TB_CONSOMMATEUR.AGE_CONSOMMATEUR as age,
              TB_ACHETEUR.NOM_ACHETEUR as nom_ach,
              SUBSTRING_INDEX(TB_ACHETEUR.EMAIL_ACHETEUR, '@', -1) as entreprise,
              TB_INFO_COMMANDE.DATE_LIVRAISON as dte_livr,
              TB_TYPE_PAIEMENT.LABEL_TYPE_PAIEMENT as type_paiement,
              TB_CIVILITE.LABEL_CIVILITE as civilite,
              TB_PLAT.LABEL_PLAT as plat,
              TB_TARIF.LABEL_TARIF as tarif
              FROM TB_COMMANDE
              INNER JOIN TB_CONSOMMATEUR ON TB_COMMANDE.ID_CONSOMMATEUR = TB_CONSOMMATEUR.ID_CONSOMMATEUR
              INNER JOIN TB_ACHETEUR ON TB_COMMANDE.ID_ACHETEUR = TB_ACHETEUR.ID_ACHETEUR
              INNER JOIN TB_CIVILITE ON TB_CONSOMMATEUR.ID_CIVILITE = TB_CIVILITE.ID_CIVILITE
              INNER JOIN TB_PLAT ON TB_COMMANDE.ID_PLAT = TB_PLAT.ID_PLAT
              INNER JOIN TB_INFO_COMMANDE ON TB_COMMANDE.ID_INFO_COMMANDE = TB_INFO_COMMANDE.ID_INFO_COMMANDE
              INNER JOIN TB_TYPE_PAIEMENT ON TB_INFO_COMMANDE.ID_TYPE_PAIEMENT = TB_TYPE_PAIEMENT.ID_TYPE_PAIEMENT
              INNER JOIN TB_TARIF ON TB_CONSOMMATEUR.ID_TARIF = TB_TARIF.ID_TARIF
              ORDER BY TB_CONSOMMATEUR.ID_CONSOMMATEUR DESC
              LIMIT ". $limit;

    // prepare query statement
    $stmt = $db->prepare($query);

    // execute query
    $stmt->execute();
    echo json_encode(returnResult($stmt));
    //return $stmt;


function returnResult($stmt) {
    $toReturn = array();
    $num = $stmt->rowCount();
    if($num>0){
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        array_push($toReturn, $row);
      }
      return $toReturn;
    }
    else{
	    return null;
      }
    }
?>