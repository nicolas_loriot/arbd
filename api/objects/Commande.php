<?php
class Commande{

    // database connection and table name
    private $conn;
    private $table_name = "TB_COMMANDE";

    // object properties
    public $id_consommateur;
    public $id_plat;
    public $id_acheteur;
    public $id_info_commande;
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function read(){

    // select all query
    $query = "SELECT p.ID_CONSOMMATEUR, p.ID_PLAT, p.ID_ACHETEUR, p.ID_INFO_COMMANDE
              FROM " . $this->table_name . " p";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // execute query
    $stmt->execute();

    return $stmt;
    }

    function create(){
    // query to insert record
    $query = "INSERT INTO " . $this->table_name . "
            SET
                ID_CONSOMMATEUR=:id_consommateur, ID_PLAT=:id_plat, ID_ACHETEUR=:id_acheteur, ID_INFO_COMMANDE=:id_info_commande";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->id_consommateur=htmlspecialchars(strip_tags($this->id_consommateur));
    $this->id_plat=htmlspecialchars(strip_tags($this->id_plat));
    $this->id_acheteur=htmlspecialchars(strip_tags($this->id_acheteur));
    $this->id_info_commande=htmlspecialchars(strip_tags($this->id_info_commande));

    // bind values
    $stmt->bindParam(":id_consommateur", $this->id_consommateur);
    $stmt->bindParam(":id_plat", $this->id_plat);
    $stmt->bindParam(":id_acheteur", $this->id_acheteur);
    $stmt->bindParam(":id_info_commande", $this->id_info_commande);
    // execute query
    if($stmt->execute()){
        return true;
    }else{
        return false;
    }

    }
}