<?php
class Consommateur{

    // database connection and table name
    private $conn;
    private $table_name = "TB_CONSOMMATEUR";

    // object properties
    public $id;
    public $first_name;
    public $last_name;
    public $age;
    public $civility;
    public $tarif;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function read(){

    // select all query
    $query = "SELECT p.ID_CONSOMMATEUR, p.NOM_CONSOMMATEUR, p.PRENOM_CONSOMMATEUR, p.AGE_CONSOMMATEUR, p.ID_TARIF, p.ID_CIVILITE
              FROM " . $this->table_name . " p";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // execute query
    $stmt->execute();

    return $stmt;
    }


    function getLatest($limit) {
      $query = "SELECT ID_CONSOMMATEUR
              FROM " . $this->table_name
	." ORDER BY " . $this->table_name . "." . "ID_CONSOMMATEUR DESC LIMIT " . $limit;

      $stmt = $this->conn->prepare($query);

      // execute query
      $stmt->execute();
      $num = $stmt->rowCount();
      //return $stmt;
      if($num>0){
	$res=array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	  extract($row);
	  $acheteur_item = $ID_CONSOMMATEUR;
	  array_push($res, $acheteur_item);
	}
	return $res;
      }
      else{
	echo null;
      }
    }

    function create(){
    // query to insert record
    $query = "INSERT INTO " . $this->table_name . "
            SET
                PRENOM_CONSOMMATEUR=:first_name, NOM_CONSOMMATEUR=:last_name, AGE_CONSOMMATEUR=:age, ID_TARIF=:tarif, ID_CIVILITE=:civility" ;

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->first_name=htmlspecialchars(strip_tags($this->first_name));
    $this->last_name=htmlspecialchars(strip_tags($this->last_name));
    $this->age=htmlspecialchars(strip_tags($this->age));
    $this->tarif=htmlspecialchars(strip_tags($this->tarif));
    $this->civility=htmlspecialchars(strip_tags($this->civility));

    // bind values
    $stmt->bindParam(":first_name", $this->first_name);
    $stmt->bindParam(":last_name", $this->last_name);
    $stmt->bindParam(":age", $this->age);
    $stmt->bindParam(":tarif", $this->tarif);
    $stmt->bindParam(":civility", $this->civility);
    // execute query
    if($stmt->execute()){
        return true;
    }else{
        return false;
    }

    }
}