<?php
class Acheteur{

    // database connection and table name
    private $conn;
    private $table_name = "TB_ACHETEUR";

    // object properties
    public $id;
    public $first_name;
    public $last_name;
    public $age;
    public $mail;
    public $civility;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function read(){

    // select all query
    $query = "SELECT p.ID_ACHETEUR, p.EMAIL_ACHETEUR, p.AGE_ACHETEUR, p.NOM_ACHETEUR, p.PRENOM_ACHETEUR, p.ID_CIVILITE
              FROM " . $this->table_name . " p";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // execute query
    $stmt->execute();

    return $stmt;
    }

    function getLatest($limit) {
      $query = "SELECT ID_ACHETEUR
              FROM " . $this->table_name
	." ORDER BY " . $this->table_name . "." . "ID_ACHETEUR DESC LIMIT " . $limit;

      $stmt = $this->conn->prepare($query);

      // execute query
      $stmt->execute();
      return $this->returnResult($stmt);
    }


    function create(){
    // query to insert record
    $query = "INSERT INTO " . $this->table_name . "
            SET
                PRENOM_ACHETEUR=:first_name, NOM_ACHETEUR=:last_name, AGE_ACHETEUR=:age, EMAIL_ACHETEUR=:mail, ID_CIVILITE=:civility";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->first_name=htmlspecialchars(strip_tags($this->first_name));
    $this->last_name=htmlspecialchars(strip_tags($this->last_name));
    $this->age=htmlspecialchars(strip_tags($this->age));
    $this->mail=htmlspecialchars(strip_tags($this->mail));
    $this->civility=htmlspecialchars(strip_tags($this->civility));

    // bind values
    $stmt->bindParam(":first_name", $this->first_name);
    $stmt->bindParam(":last_name", $this->last_name);
    $stmt->bindParam(":mail", $this->mail);
    $stmt->bindParam(":civility", $this->civility);
    $stmt->bindParam(":age", $this->age);
    // execute query
    if($stmt->execute()){
        return true;
    }else{
        return false;
    }

    }

    function returnResult($stmt) {
      $num = $stmt->rowCount();
      //return $stmt;
      if($num>0){
	$acheteurs_arr=array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	  extract($row);
	  $acheteur_item = $ID_ACHETEUR;
	  array_push($acheteurs_arr, $acheteur_item);
	}
	return $acheteurs_arr;
      }
      else{
	return null;
      }
    }
}