<?php
class InfoCommande{

    // database connection and table name
    private $conn;
    private $table_name = "TB_INFO_COMMANDE";

    // object properties
    public $id;
    public $date_livraison;
    public $type_paiement;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function read(){

    // select all query
    $query = "SELECT p.ID_INFO_COMMANDE, p.EMAIL_ACHETEURDATE_LIVRAISON, p.ID_TYPE_PAIEMENT
              FROM " . $this->table_name . " p";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // execute query
    $stmt->execute();

    return $stmt;
    }

        function getLatest($limit) {
      $query = "SELECT ID_INFO_COMMANDE
              FROM " . $this->table_name
	." ORDER BY " . $this->table_name . "." . "ID_INFO_COMMANDE DESC LIMIT " . $limit;

      $stmt = $this->conn->prepare($query);

      // execute query
      $stmt->execute();
      $num = $stmt->rowCount();
      //return $stmt;
      if($num>0){
	$res=array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	  extract($row);
	  $acheteur_item = $ID_INFO_COMMANDE;
	  array_push($res, $acheteur_item);
	}
	return $res;
      }
      else{
	echo null;
      }
    }

    function create(){
    // query to insert record
      $query = "INSERT INTO " . $this->table_name . "
            SET
                DATE_LIVRAISON=:date_livraison, ID_TYPE_PAIEMENT=:type_paiement";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->date_livraison=htmlspecialchars(strip_tags($this->date_livraison));
    $this->type_paiement=htmlspecialchars(strip_tags($this->type_paiement));
    // bind values
    $stmt->bindParam(":date_livraison", $this->date_livraison);
    $stmt->bindParam(":type_paiement", $this->type_paiement);
    // execute query
    if($stmt->execute()){
        return true;
    }else{
        return false;
    }

    }
}