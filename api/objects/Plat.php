<?php
class Plat{

    // database connection and table name
    private $conn;
    private $table_name = "TB_PLAT";

    // object properties
    public $id;
    public $label_plat;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    function read(){

    // select all query
    $query = "SELECT p.ID_PLAT, p.LABEL_PLAT
              FROM " . $this->table_name . " p";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // execute query
    $stmt->execute();

    return $this->returnResult($stmt);
    }

    function returnResult($stmt) {
      $num = $stmt->rowCount();
      //return $stmt;
      if($num>0){
	$plats_arr=array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	  extract($row);
	  $plat_item = array(
			     "id_plat" => $ID_PLAT,
			     "label_plat" => $LABEL_PLAT);
	  array_push($plats_arr, $plat_item);
	}
	return $plats_arr;
      }
      else{
	return null;
      }
    }
}