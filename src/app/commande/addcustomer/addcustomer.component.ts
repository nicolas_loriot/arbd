import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/primeng';

import { CustomerDto } from '../customer-dto';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.component.html',
  styleUrls: ['./addcustomer.component.css']
})
export class AddcustomerComponent implements OnInit {
  @Output() updateCustomer = new EventEmitter();
  customer: CustomerDto;
  id: number;
  display: boolean = false;
  
  
  constructor() { }

  ngOnInit() {
    this.customer = new CustomerDto();
  }

    pushCustomer = function() {
      if (this.customer.nom != "" && this.customer.prenom && this.customer.age){
        this.updateCustomer.emit(this.customer)
        this.customer = new CustomerDto();
        this.display = false; 
      }
    }
    showDialog() {
        this.display = true;
    }
}
