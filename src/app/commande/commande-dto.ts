import { CustomerDto } from './customer-dto';

export class CommandeDto {
    customer: CustomerDto = new CustomerDto();
    meal: string = "";
}
