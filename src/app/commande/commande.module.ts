import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { CommandeRoutingModule } from './commande-routing.module';
import { OrderComponent } from './order/order.component';
import { PaymentComponent } from './payment/payment.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { AddcustomerComponent } from './addcustomer/addcustomer.component';
import { CustomerDto } from './customer-dto';
import { CommandeDto } from './commande-dto';

import { StatServiceService } from '../statistiques/stat-service.service';

import {ButtonModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {DataListModule} from 'primeng/primeng';
import {DataTableModule} from 'primeng/primeng';
import {SharedModule} from 'primeng/primeng';
import {BlockUIModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    CommandeRoutingModule,
    ButtonModule,
    DropdownModule,
    InputTextModule,
    FormsModule,
    GrowlModule,
    DialogModule,
    DataListModule,
    DataTableModule,
    SharedModule,
    BlockUIModule,
    HttpClientModule
  ],
  declarations: [OrderComponent, PaymentComponent, ConfirmationComponent, AddcustomerComponent],
  providers: [StatServiceService],
  bootstrap: [OrderComponent]
})
export class CommandeModule { }
