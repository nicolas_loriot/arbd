import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { SelectItem } from 'primeng/primeng';
import { Message } from 'primeng/components/common/api';
import {Header} from 'primeng/primeng';
import {Footer} from 'primeng/primeng';

import { CustomerDto } from '../customer-dto';
import { CommandeDto } from '../commande-dto';

import { StatServiceService } from '../../statistiques/stat-service.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  public err: string;
  public nb_results;
  public totalRecords: number;
  public datas: any
  private url: string;
  public blocked: boolean;
  public body: any = {res: 0, order: 'ASC'}
  constructor(private http: HttpClient,
              private statService: StatServiceService) { }

  ngOnInit() {
    this.err = ""
    this.totalRecords = 0;
    this.nb_results = 10;
    this.datas = null;
    this.blocked = true;
    this.url = this.statService.base_url + 'allCommands.php'
    this.getData()
  }
  getData() {
    this.blocked = true;
    if (this.nb_results < 1 || this.nb_results > 90000)
      return (this.getError());
    this.err = ""; 
    this.http.post(this.url, this.nb_results).subscribe(data => 
    {
      console.log(data), 
      this.datas = data, 
      this.blocked = false,
      this.totalRecords = this.datas.length
    });
  }

  getError() {
    if (this.nb_results < 1)
      this.err = "Saisissez une valeur positive"
    else
      this.err = "Pense aux ressources: a tu réellement besoinde tout cela? (max 90000)"
    this.blocked = false
  }
}
