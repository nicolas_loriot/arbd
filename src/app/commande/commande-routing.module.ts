import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderComponent } from './order/order.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { PaymentComponent } from './payment/payment.component';

const commanderoutes: Routes = [
  {
    path: '',
    component: OrderComponent
  },
  {
    path: 'order',
    component: OrderComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(commanderoutes)],
  exports: [RouterModule],
  providers: []
})
export class CommandeRoutingModule { }

export const routing = RouterModule.forRoot(commanderoutes);

export const routedComponents = [
  PaymentComponent,
  OrderComponent,
  ConfirmationComponent
]
