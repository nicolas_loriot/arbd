import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatviewerComponent } from './statviewer.component';

describe('StatviewerComponent', () => {
  let component: StatviewerComponent;
  let fixture: ComponentFixture<StatviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
