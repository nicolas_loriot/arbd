import { Component, OnInit, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { StatServiceService } from '../../stat-service.service';
import { CharType } from '../../char-type'

import { SelectItem } from 'primeng/primeng'

@Component({
  selector: 'app-numofmenubyfirm',
  templateUrl: './numofmenubyfirm.component.html',
  styleUrls: ['./numofmenubyfirm.component.css']
})
export class NumofmenubyfirmComponent implements OnInit {

  data: any;
  datas: any;
  dte: any;
  charType: SelectItem[];
  selectedCharType: SelectItem;
  t: any;
  private options: any;
  private url: string;
  constructor(private statService: StatServiceService,
              private http: HttpClient) { }

  ngOnInit() {
    this.url = this.statService.base_url + 'menuByFirm.php'
    this.options = this.statService.options
    this.getData();
    this.setCharType();
    this.t = new EventEmitter();
  }

  private updateSelectedType(type: number){
    this.selectedCharType = this.charType[type];
    this.init_chart();
  }

  private setCharType() {
    this.selectedCharType = {label: 'doughnut', value: 1}
    this.charType = [];
    this.charType.push({label: 'pie', value: 0});
    this.charType.push({label: 'doughnut', value: 1});
    this.charType.push({label: 'bar', value: 2});
  }
  private init_chart (){
    this.data = this.statService.getChart(this.datas, this.selectedCharType.value)
  }

  private pushInDatas() {

  }
  getData() {
    console.log(this.url)
    this.http.get(this.url).subscribe(data => {
     this.datas = data;
      this.init_chart();
      console.log(data)
    }); 
  }

  private updateData() {
    this.http.get(this.url).subscribe(data => {
      this.data = data;
      this.init_chart();
      console.log(this.datas)
    });
  }
}
