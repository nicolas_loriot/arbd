import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumofmenubyfirmComponent } from './numofmenubyfirm.component';

describe('NumofmenubyfirmComponent', () => {
  let component: NumofmenubyfirmComponent;
  let fixture: ComponentFixture<NumofmenubyfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumofmenubyfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumofmenubyfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
