import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerbymenuComponent } from './customerbymenu.component';

describe('CustomerbymenuComponent', () => {
  let component: CustomerbymenuComponent;
  let fixture: ComponentFixture<CustomerbymenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerbymenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerbymenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
