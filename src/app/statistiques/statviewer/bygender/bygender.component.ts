import { Component, OnInit, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { StatServiceService } from '../../stat-service.service';
import { CharType } from '../../char-type'

import { SelectItem } from 'primeng/primeng'
@Component({
  selector: 'app-bygender',
  templateUrl: './bygender.component.html',
  styleUrls: ['./bygender.component.css']
})
export class BygenderComponent implements OnInit {

   
  private data: any;
  private datas = {data:[], labels:[], percents:[]};
  private dte: any;
  private charType: SelectItem[];
  private selectedCharType: SelectItem;
  private t: any;
  private url: string;
  private p_male: number;
  private p_female: number;
  private percents: any[];
  private options: any;
  constructor(private statService: StatServiceService,
              private http: HttpClient) { }

  ngOnInit() {
    this.options = this.statService.options
    this.p_male = 0;
    this.p_female = 0;
    this.setCharType();
    this.url = this.statService.base_url + 'byGender.php'
    this.getData();
    this.t = new EventEmitter();
  }

  private updateSelectedType(type: number){
    this.selectedCharType = this.charType[type];
    this.init_chart();
  }

  private setCharType() {
    this.selectedCharType = {label: 'doughnut', value: 1}
    this.charType = [];
    this.charType.push({label: 'pie', value: 0});
    this.charType.push({label: 'doughnut', value: 1});
    this.charType.push({label: 'bar', value: 2});
    console.log(this.charType)
  }
  private init_chart (){
    this.data = this.statService.getChart(this.datas, this.selectedCharType.value)
  }

  private pushInDatas() {

  }
  getData() {
    this.http.get(this.url).subscribe(data => {
      this.dte = data;
      this.datas.data.push(this.dte.nb_male);
      this.datas.data.push(this.dte.nb_femal);
      this.datas.labels.push(this.dte.male);
      this.datas.labels.push(this.dte.femal);
      this.init_chart();
      this.getPercents(this.datas.data[0], this.datas.data[1]);
      this.datas.percents.push(this.p_male);
      this.datas.percents.push(this.p_female);
      this.fillPercents(this.datas)
      console.log(this.datas)
    }); 
  }
  getPercents(nb_male: number, nb_femal: number) {
    this.p_male = nb_male / (nb_male + nb_femal);
    this.p_female = nb_femal / (nb_femal + nb_male);
  }

  fillPercents(datas: any) {
    
    this.percents = [];
    for (let i = 0; i < datas.data.length; i++) {
      let temp: any = {Sexe: '', Nombre: '', Pourcentage: ''};
      temp.Sexe = datas.labels[i];
      temp.Nombre = datas.data[i];
      temp.Pourcentage = Math.floor(datas.percents[i] * 100 ) + " %";
      this.percents.push(temp);
    }
  }
  private updateData() {
    this.http.get(this.url).subscribe(data => {
      this.dte = data;
      this.datas.data[0] = this.dte.nb_male;
      this.datas.data[1] = this.dte.nb_femal;
      this.init_chart();
      console.log(this.datas)
    });
  }

}
