import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BygenderComponent } from './bygender.component';

describe('BygenderComponent', () => {
  let component: BygenderComponent;
  let fixture: ComponentFixture<BygenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BygenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BygenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
