import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByageComponent } from './byage.component';

describe('ByageComponent', () => {
  let component: ByageComponent;
  let fixture: ComponentFixture<ByageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
