import { Component, OnInit, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { StatServiceService } from '../../stat-service.service';
import { CharType } from '../../char-type'

import { SelectItem } from 'primeng/primeng'

@Component({
  selector: 'app-byage',
  templateUrl: './byage.component.html',
  styleUrls: ['./byage.component.css']
})
export class ByageComponent implements OnInit {
  
  data: any;
  datas = {data:[], labels:[]};
  dte: any;
  charType: SelectItem[];
  selectedCharType: SelectItem;
  t: any;
  private options: any;
  private url: string;

  constructor(private statService: StatServiceService,
              private http: HttpClient) { }

  ngOnInit() {
    this.options = this.statService.options
    this.url = this.statService.base_url + 'byAge.php'
    this.getData();
    this.setCharType();
    this.t = new EventEmitter();
  }

  private updateSelectedType(type: number){
    this.selectedCharType = this.charType[type];
    this.init_chart();
  }

  private setCharType() {
    this.selectedCharType = {label: 'doughnut', value: 1}
    this.charType = [];
    this.charType.push({label: 'pie', value: 0});
    this.charType.push({label: 'doughnut', value: 1});
    this.charType.push({label: 'bar', value: 2});
  }
  private init_chart (){
    this.data = this.statService.getChart(this.datas, this.selectedCharType.value)
  }

  private pushInDatas() {

  }
  getData() {
    this.http.get(this.url).subscribe(data => {
      this.dte = data;
      this.datas.data.push(this.dte._18);
      this.datas.data.push(this.dte._18_25);
      this.datas.data.push(this.dte._26_39);
      this.datas.data.push(this.dte._40_54);
      this.datas.data.push(this.dte._55);
      this.datas.labels.push(this.dte.str_18);
      this.datas.labels.push(this.dte.str_18_25);
      this.datas.labels.push(this.dte.str_26_39);
      this.datas.labels.push(this.dte.str_40_54);
      this.datas.labels.push(this.dte.str_55);
      this.init_chart();
      console.log(data)
    }); 
  }

  private updateData() {
    this.http.get(this.url).subscribe(data => {
      this.dte = data;
      this.datas.data[0] = (this.dte._18);
      this.datas.data[1] = (this.dte._18_25);
      this.datas.data[2] = (this.dte._26_39);
      this.datas.data[3] = (this.dte._40_54);
      this.datas.data[4] = (this.dte._55);
      this.init_chart();
      console.log(this.datas)
    });
  }

}
