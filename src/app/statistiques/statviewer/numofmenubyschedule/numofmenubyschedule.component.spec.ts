import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumofmenubyscheduleComponent } from './numofmenubyschedule.component';

describe('NumofmenubyscheduleComponent', () => {
  let component: NumofmenubyscheduleComponent;
  let fixture: ComponentFixture<NumofmenubyscheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumofmenubyscheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumofmenubyscheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
