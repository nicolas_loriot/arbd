import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';

import { StatistiquesRoutingModule } from './statistiques-routing.module';

import { BygenderComponent } from './statviewer/bygender/bygender.component';
import { ByageComponent } from './statviewer/byage/byage.component';
import { CustomerbymenuComponent } from './statviewer/customerbymenu/customerbymenu.component';
import { NumofmenubyscheduleComponent } from './statviewer/numofmenubyschedule/numofmenubyschedule.component';
import { NumofmenubyfirmComponent } from './statviewer/numofmenubyfirm/numofmenubyfirm.component';
import { StatviewerComponent } from './statviewer/statviewer.component';

import { TabViewModule, ButtonModule, ChartModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import {DataTableModule,SharedModule} from 'primeng/primeng';

import { StatServiceService } from './stat-service.service';

@NgModule({
  imports: [
    CommonModule,
    StatistiquesRoutingModule,
    TabViewModule,
    ButtonModule,
    ChartModule,
    DropdownModule,
    HttpClientModule,
    FormsModule,
    DataTableModule,
    SharedModule
  ],
  declarations: [BygenderComponent, ByageComponent, CustomerbymenuComponent, NumofmenubyscheduleComponent, NumofmenubyfirmComponent, StatviewerComponent],
  providers: [StatServiceService]
})
export class StatistiquesModule { }
