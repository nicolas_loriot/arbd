import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StatviewerComponent } from './statviewer/statviewer.component'
import { ByageComponent } from './statviewer/byage/byage.component'
import { CustomerbymenuComponent } from './statviewer/customerbymenu/customerbymenu.component'
import { NumofmenubyfirmComponent } from './statviewer/numofmenubyfirm/numofmenubyfirm.component'
import { NumofmenubyscheduleComponent } from './statviewer/numofmenubyschedule/numofmenubyschedule.component'

const statroutes: Routes = [
    {
      path: '',
      component: StatviewerComponent
    },
    {
      path: 'statistiques',
      component: StatviewerComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(statroutes)],
  exports: [RouterModule],
  providers: []
})
export class StatistiquesRoutingModule { }
