import { Injectable } from '@angular/core';

import { CharType } from './char-type';

@Injectable()
export class StatServiceService {

  privatColors: string[]
  public base_url;
  public options: any;
  constructor() { 
    this.base_url = 'http://localhost/api/stat/';
    this.privatColors = [];
    this.privatColors.push("#FF6384");
    this.privatColors.push("#36A2EB");
    this.privatColors.push("#FFCE56");
    this.privatColors.push("#f442e8");
    this.privatColors.push("#10e8dd");
    this.privatColors.push("#ea0f0f");
    this.privatColors.push("#0f0101");
    this.privatColors.push("#dba90e");
    this.privatColors.push("#2ed639");
    this.privatColors.push("#1417e1");
    this.options = {
            title: {
                display: false,
            },
            legend: {
                display: false,
            }
        };
  }

  getChart(datas: any, type: number): any {
    let c: CharType = new CharType()
    let data: any;
    if (datas != undefined) {
      if (type == c.bar) {
        data = this.getBarChart(datas);
        this.options.legend.display = false;
      }
      else if (type == c.doughnut || type == c.pie) {
        data = this.getPieChart(datas)
        this.options.legend.display = true;
      }
    }
    return data;
  }

  getBarChart(datas: any): any {
    return this.getPieChart(datas);
  }

  getPieChart(datas: any):any {
    if (datas != undefined) {
      let data: any = {
            labels: datas.labels,
            datasets: [
                {
                    data: datas.data,
                    backgroundColor: this.setColors(datas.labels),
                    hoverBackgroundColor: this.setColors(datas.labels),
                }]    
            };
      return data;
    }
  }

  private setColors(dte: string[]): string[] {
    
    let colors: string[] = [];
    if (dte != undefined && dte.length) {
      let i = 0;
      dte.forEach(element => {
        colors.push(this.privatColors[i])
        i = i + 1;
      });
    }
    return colors;
  }
}
