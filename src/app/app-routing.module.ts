import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component'

import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [{
        path: '', loadChildren:'./commande/commande.module#CommandeModule'
      },
      {
        path: 'order', loadChildren:'./commande/commande.module#CommandeModule'
      },
      {
        path: 'statistiques', loadChildren: './statistiques/statistiques.module#StatistiquesModule'        
      }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class AppRoutingModule { }


