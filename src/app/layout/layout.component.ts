import { Component, OnInit } from '@angular/core';

import { StepsModule, MenuItem, TabMenuModule, MenubarModule } from 'primeng/primeng';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

 items: MenuItem[];
  constructor() { }

  ngOnInit() {
    this.items = [
            {label: 'Commande', routerLink: ['/order']},
            {label: 'Statistiques', routerLink: ['/statistiques']},
        ];
  }

}
